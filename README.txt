CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Colorbox Media Video extends Drupal Colorbox module with support for core media video and
remote video types.


FEATURES:
---------

The Colorbox Media Video module:

* Works as a Formatter for Video URL field in Remote Media Video
* Choose between a text or thumbnail to launch a Colorbox
* Supports Media thumbnail and image styles
* Supports Gallery (video grouping) - aka Colorbox rel



REQUIREMENTS
------------

Colorbox (https://www.drupal.org/project/colorbox)
Media (core)


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8

2. Go to "Media types" -> "Remote Video" -> "Manage display" -> "Video URL"
=> select Colorbox Remote Media Video formatter and adjust settings



MAINTAINERS
-----------

Current maintainers:

 * Tamer Zoubi (https://www.drupal.org/u/tamerzg)

